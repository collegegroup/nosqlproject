# NoSQL

## Status
El proyecto se encuentra actualmente detenido. Lo que van a encontrar en este momento es un simple proyecto que se ejecuta de manera local. En un futuro se va a montar la aplicación en la nube para que todos tengan acceso.

## Descripcion

¡Bienvenido al proyecto de base de datos no relacional con Java SpringBoot, MongoDB y Redis Server!

En este emocionante proyecto, hemos creado una potente aplicación respaldada por una base de datos no relacional y un servidor de almacenamiento en caché. La combinación de Java SpringBoot como backend, MongoDB como base de datos y Redis como servidor de caché nos proporciona una solución escalable y de alto rendimiento para manejar grandes cantidades de datos y solicitudes concurrentes.

En esta base de datos no relacional, podemos aprovechar las ventajas de MongoDB para almacenar datos de manera flexible, sin la necesidad de cumplir con una estructura rígida. Además, utilizamos Redis Server para acelerar las operaciones de lectura y escritura mediante el almacenamiento en caché de datos frecuentemente solicitados, lo que mejora drásticamente la velocidad de acceso a la información.

Nuestra aplicación aprovecha al máximo la versatilidad de Java SpringBoot, lo que nos permite desarrollar rápidamente funciones complejas y garantizar una base sólida para la lógica del negocio. Con la combinación de estas tecnologías, estamos listos para enfrentar cualquier desafío que se presente en cuanto a escalabilidad, rendimiento y disponibilidad.

En este archivo Readme, encontrarás información valiosa sobre cómo configurar el entorno de desarrollo, las dependencias necesarias, instrucciones de instalación y cómo utilizar los endpoints proporcionados por nuestra API para interactuar con la base de datos no relacional y el servidor de caché.

¡Esperamos que este proyecto te inspire y te ayude a comprender las ventajas de trabajar con una base de datos no relacional como MongoDB y un servidor de caché como Redis en combinación con Java SpringBoot!

Gracias por formar parte de esta emocionante experiencia y estamos ansiosos por ver cómo llevas este proyecto al siguiente nivel. Si tienes alguna pregunta o inquietud, no dudes en comunicarte con nuestro equipo.

¡Vamos a comenzar a explorar el fascinante mundo de las bases de datos no relacionales y la optimización de rendimiento con Redis!

## Instalacion

### Paso 1: Instalar Java 14

Para instalar Java 14 en tu sistema, sigue estos pasos:

Paso 1: Verificar requisitos
Asegúrate de que tu sistema cumpla con los requisitos para instalar Java 14. Java 14 es compatible con una variedad de sistemas operativos, incluyendo Windows, macOS y diversas distribuciones de Linux.

Paso 2: Descargar el JDK
Ve al sitio oficial de Oracle para descargar el JDK (Kit de Desarrollo de Java) 14 o utiliza un proveedor de OpenJDK como AdoptOpenJDK, Amazon Corretto o Azul Zulu. Asegúrate de seleccionar la versión adecuada para tu sistema operativo y arquitectura (32 o 64 bits).

Paso 3: Instalar el JDK
Una vez que hayas descargado el archivo de instalación, sigue las instrucciones específicas para tu sistema operativo:

Windows: Ejecuta el archivo de instalación (.exe) y sigue los pasos del asistente de instalación.
macOS: Ejecuta el archivo de instalación (.dmg) y sigue las instrucciones para arrastrar el JDK a la carpeta de aplicaciones.
Linux: Extrae el archivo tar.gz descargado y sigue las instrucciones de instalación proporcionadas en el sitio web del proveedor de OpenJDK que hayas elegido.
Paso 4: Configurar las variables de entorno
Es importante configurar las variables de entorno para que el sistema pueda encontrar la instalación de Java. Los pasos para hacerlo varían según el sistema operativo:

Windows: Agrega la ubicación del directorio bin del JDK a la variable de entorno "PATH".
macOS y Linux: Configura la variable de entorno "JAVA_HOME" para que apunte al directorio de instalación del JDK y agrega "$JAVA_HOME/bin" a la variable de entorno "PATH".
Paso 5: Verificar la instalación
Abre una nueva ventana del terminal o símbolo del sistema y ejecuta el siguiente comando para verificar que Java se haya instalado correctamente:

java -version

Deberías ver la versión de Java instalada (en este caso, Java 14) en la salida del comando.

### Paso 2: Instalar MongoDB

Para instalar MongoDB de forma gratuita, sigue estos pasos:

Paso 1: Descargar MongoDB
Ve al sitio oficial de MongoDB para descargar la versión Community Server, que es gratuita y adecuada para la mayoría de los proyectos. Puedes acceder a la página de descargas en el siguiente enlace: https://www.mongodb.com/try/download/community

Paso 2: Seleccionar el sistema operativo
Una vez en la página de descargas, elige tu sistema operativo de la lista desplegable. MongoDB está disponible para Windows, macOS y diversas distribuciones de Linux.

Paso 3: Descargar el archivo de instalación
Haz clic en el enlace de descarga para el sistema operativo que estás utilizando y espera a que se complete la descarga del archivo de instalación.

Paso 4: Instalar MongoDB

Windows:
Ejecuta el archivo de instalación (.msi) descargado.
Sigue el asistente de instalación y selecciona las opciones que desees.
Asegúrate de marcar la casilla que dice "Install MongoDB Compass", que es una interfaz gráfica útil para administrar la base de datos.
Completa la instalación y MongoDB estará listo para usar.

macOS:
Abre el archivo .tgz descargado.
Extrae el contenido del archivo y colócalo en la ubicación deseada.
Asegúrate de configurar correctamente las variables de entorno PATH para acceder a los comandos de MongoDB desde cualquier ubicación en la terminal.

Linux:
Abre una terminal y navega a la ubicación del archivo .tgz descargado.
Extrae el contenido del archivo usando el siguiente comando:

tar -zxvf nombre_del_archivo.tgz

Mueve la carpeta resultante a la ubicación deseada y configura las variables de entorno PATH adecuadamente.

Paso 5: Verificar la instalación
Abre una nueva ventana del terminal o símbolo del sistema y ejecuta el siguiente comando para verificar que MongoDB se haya instalado correctamente:

mongo --version
Deberías ver la versión de MongoDB instalada en la salida del comando

### Paso 3: Instalar Servidor Redis

A partir de mi conocimiento actual (septiembre de 2021), la página oficial de Redis (https://redis.io/download/) ofrece descargas de versiones precompiladas para Linux y macOS, pero no para Windows. Sin embargo, existe un proyecto conocido como "Redis on Windows" que proporciona una versión compatible con Windows.

A continuación, estos son los pasos para instalar Redis Server en Windows utilizando "Redis on Windows":

Paso 1: Descargar Redis on Windows
Visita el repositorio de "Redis on Windows" en GitHub: https://github.com/MicrosoftArchive/redis/releases

Busca la última versión disponible y descarga el archivo ZIP correspondiente. Deberías descargar un archivo que incluya "Redis-x64-{versión}.zip" o "Redis-x86-{versión}.zip", dependiendo de si tu sistema es de 64 o 32 bits, respectivamente.

Paso 2: Extraer el archivo
Descomprime el archivo ZIP descargado en una ubicación deseada en tu sistema.

Paso 3: Configurar el servidor Redis
En la carpeta donde descomprimiste Redis, deberías encontrar el ejecutable "redis-server.exe".

Paso 4: Iniciar el servidor Redis
Abre una ventana del símbolo del sistema (CMD) con privilegios de administrador y navega hasta la ubicación donde se encuentra "redis-server.exe". Luego, ejecuta el siguiente comando para iniciar el servidor Redis:

redis-server.exe

El servidor Redis debería iniciarse y quedar en ejecución, listo para aceptar conexiones.

Paso 5: Verificar la instalación
Para asegurarte de que Redis se haya instalado correctamente, puedes conectar al servidor Redis ejecutando el siguiente comando en otra ventana del símbolo del sistema:

redis-cli ping

Si Redis responde con "PONG", significa que está en funcionamiento y listo para recibir comandos.

## Ejecucion del proyecto

1- Iniciamos la base de datos MongoDB.
2- Iniciamos el servidor redis.
Navegamos hasta la ubicación donde se encuentra "redis'server.exe" y lo ejecutamos como administrador.
3- Desde la consola, nos ubicamos en la carpeta "tarea2" y ejecutamos los comandos de gradle para construir el proyecto-
Iniciamos con "gradlew clean" para iniciar de cero.
Luego, ejecutamos "gradlew bootRun" para construir y ejecutar la aplicación SpringBoot

¡Listo! La aplicación se encuentra ejecutando. Hay un endpoint habilitado para realizar llamadas utilizando la herramienta Postman.


## Interactuamos con la api utilizando Postman

En el proyecto van a encontrar un archivo con algunos ejemplos que construimos para probar la aplicación. Sirven de ejemplo para que entiendas como interactuar con la aplicación.

### NOTA: Se puede abrir un redis-cli y con el comando MONITOR se pueden ver las operaciones que realiza el Cache.

### Comandos utilizados: KEYS [pattern], MONITOR, FLUSHALL

## Soporte
De momento, la aplicación no está recibiendo soporte.

## Trabajo a futuro
La aplicación fue construida para simular el funcionamiento de una aplicación similar a Twitter. Actualmente no cuenta con sitio web ni aplicación movil. Además, se agregaría un sistema de notificaciones y de chat.

## Autores y agradecimientos
Para este proyecto trabajé con Tomás Pascal y Mathías Castro durante el tiempo que cursamos la carrear de "Tecnólogo en Informática" durante 2020.
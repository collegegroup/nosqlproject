package com.nosql.controllers;

import com.nosql.services.ComentariosService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/comentarios")
public class ComentariosController {

    @Autowired
    private ComentariosService service;
    
    @RequestMapping(value = "/altaComentario/{email}", method = RequestMethod.POST)
    public ResponseEntity<Object> altaComentario(@PathVariable("email") String email,@RequestBody String  comentario) {
        return new ResponseEntity<>(service.altaComentario(email, comentario),HttpStatus.OK);
    }  

    @RequestMapping(value = "/listarComentarios/{email}", method = RequestMethod.GET)
    public ResponseEntity<Object> listarComentarios(@PathVariable("email") String email) {
        return new ResponseEntity<>(service.listarComentarios(email), HttpStatus.OK);
    }  

    @RequestMapping(value = "/agregarEmocion/{comentario}/{email}/{meGusta}", method = RequestMethod.PUT)
    public ResponseEntity<Object> agregarEmocion(@PathVariable("email") String email, @PathVariable("comentario") ObjectId comentario, @PathVariable("meGusta") Boolean meGusta) {
        return new ResponseEntity<>(service.agregarEmocion(email, comentario, meGusta), HttpStatus.OK);
    } 

    @RequestMapping(value = "/leerComentario/{comentario}", method = RequestMethod.GET)
    public ResponseEntity<Object> leerComentario(@PathVariable("comentario") ObjectId comentario) {
        return new ResponseEntity<>(service.leerComentario(comentario), HttpStatus.OK);
    }
  
}
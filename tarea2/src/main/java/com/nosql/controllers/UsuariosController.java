package com.nosql.controllers;

import com.nosql.services.UsuariosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/usuarios")
public class UsuariosController {

    @Autowired
    private UsuariosService service;

    @RequestMapping(value = "/altaUsuario/{email}", method = RequestMethod.POST)
    public ResponseEntity<Object> altaUsuario(@PathVariable("email") String email) {
        return new ResponseEntity<>(service.altaUsuario(email), HttpStatus.OK);     
    }  
}
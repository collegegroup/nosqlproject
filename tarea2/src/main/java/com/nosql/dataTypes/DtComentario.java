package com.nosql.dataTypes;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DtComentario {

    public String id;
    public String comentario;
    public String usuario;
    public Integer meGusta;
    public Integer noMeGusta;

}

package com.nosql.repositories;

import com.nosql.models.Comentario;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ComentariosRepository extends MongoRepository<Comentario, ObjectId> {
  @Cacheable(value = "comentarioId")
  Optional<Comentario> findByid(ObjectId _id);	

  @CachePut(value = "comentarioId", key = "#result.id")
  <S extends Comentario> S save(S comentario);
  
}

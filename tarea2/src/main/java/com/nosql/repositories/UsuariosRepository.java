package com.nosql.repositories;

import com.nosql.models.Usuario;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface UsuariosRepository extends MongoRepository<Usuario, ObjectId> {
  Optional<Usuario> findById(ObjectId id);

  @Cacheable(value = "usuarioEmail")	
  Optional<Usuario> findByEmail(String email);

  @CachePut(value = "usuarioEmail", key = "#result.email")
  <S extends Usuario> S save(S user);

  }

package com.nosql.models;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Usuario implements Serializable {    

    private static final long serialVersionUID = 1L;
    
    @Id
    public ObjectId id;
    @Indexed
    public String email;
    public List<ObjectId> comentarios;

}

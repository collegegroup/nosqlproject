package com.nosql.models;

import java.io.Serializable;
import java.util.List;

import com.nosql.dataTypes.DtComentario;
import com.nosql.repositories.UsuariosRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Comentario implements Serializable {

    @Autowired
    UsuariosRepository repository;

    private static final long serialVersionUID = 1L;

    @Id
	public ObjectId id;
    public String comentario;
    public ObjectId usuarioId;
    public List<ObjectId> meGusta;
    public List<ObjectId> noMeGusta;

    //no esta antando, dice que repository es null
    public DtComentario toEnum(String email){
        DtComentario ret = new DtComentario();
        ret.id = this.id.toString();
        ret.comentario = this.comentario;
        ret.usuario = email;
        ret.meGusta = this.meGusta.size();
        ret.noMeGusta = this.noMeGusta.size();
        return ret;
    }
}

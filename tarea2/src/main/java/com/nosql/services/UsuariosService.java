package com.nosql.services;

import java.util.ArrayList;
import java.util.Optional;

import com.nosql.models.Usuario;
import com.nosql.repositories.UsuariosRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository repository;

    public Usuario altaUsuario(String email) {
        Optional<Usuario> existe = repository.findByEmail(email);
        if(!existe.isPresent()){      
            Usuario usuario = new Usuario();
            usuario.id = ObjectId.get();
            usuario.email = email;
            usuario.comentarios = new ArrayList<ObjectId>();
            repository.save(usuario);
            return usuario;
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }     
    }
    
}

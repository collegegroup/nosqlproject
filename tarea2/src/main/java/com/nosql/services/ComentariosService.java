package com.nosql.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.nosql.dataTypes.DtComentario;
import com.nosql.models.Comentario;
import com.nosql.models.Usuario;
import com.nosql.repositories.ComentariosRepository;
import com.nosql.repositories.UsuariosRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ComentariosService {

    @Autowired
    private ComentariosRepository repository;
    @Autowired
    private UsuariosRepository urepository;
    
    public DtComentario altaComentario(String email, String comentario) {
        Optional<Usuario> existe = urepository.findByEmail(email);    

        if(existe.isPresent()){      
            if(comentario.length() > 256){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }else{
                Comentario aux = new Comentario();
                aux.id = ObjectId.get();
                aux.comentario = comentario;
                aux.usuarioId = existe.get().id;
                aux.meGusta = new ArrayList<ObjectId>();
                aux.noMeGusta = new ArrayList<ObjectId>();
                Usuario user = existe.get();
                user.comentarios.add(aux.id);
                urepository.save(user);
                repository.save(aux);
                return aux.toEnum(user.email);
            }
        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }     
    }

    public List<DtComentario> listarComentarios(String email) {
        Optional<Usuario> existe = urepository.findByEmail(email);    
            
        if(existe.isPresent()){
            List<ObjectId> comentarios = existe.get().comentarios;
            List<DtComentario> ret = new ArrayList<>(); 
            for( ObjectId id : comentarios){
                Comentario aux = repository.findByid(id).get();
                ret.add(aux.toEnum(email));
            }
            return ret;

        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }     

    public DtComentario agregarEmocion(String email, ObjectId comentario, boolean meGusta) {
        Optional<Usuario> existe = urepository.findByEmail(email);    
        Optional<Comentario> existeCom = repository.findByid(comentario);

        if(existe.isPresent()){

            if(existeCom.isPresent()){
                if(meGusta)
                    existeCom.get().meGusta.add(existe.get().id);
                else
                    existeCom.get().noMeGusta.add(existe.get().id);
                repository.save(existeCom.get());
                return existeCom.get().toEnum(existe.get().email);

            }else{
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }                

        }else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }     
    }

    public DtComentario leerComentario(ObjectId comentario){

        Optional<Comentario> existe = repository.findByid(comentario);    
            if(existe.isPresent()){           
                String email = urepository.findById(existe.get().usuarioId).get().email;
                return existe.get().toEnum(email);  
            }else{
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            } 
    }
}